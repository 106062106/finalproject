function NewGame() {
  var width = window.innerWidth;
  var height = window.innerHeight;

  if (width < 640) width = 640;
  else width -= 20;
  if (height < 480) height = 480;
  else height -= 20;

  var game = new Phaser.Game(width, height, Phaser.AUTO, 'theGame');
  console.log("Phaser initializing...");
  console.log("Width: " + width);
  console.log("Height: " + height);

  game.state.add('boot', bootState);
  game.state.add('load', loadState);
  game.state.add('menu', menuState);
  game.state.add('map02', map02);
  game.state.add('tutorial', tutorialState);
  game.state.start('boot');

  return game;
}

game = NewGame();