var tutorialState = {
  preload: function() {
    game.stage.backgroundColor = '#CCCCCC';
    this.walls = initWalls();
    this.keys = setupWASD();
  },
  create: function() {
    this.player = setupPlayer(game.width/2, game.height/2, 'playerSheet');
    addWall(game.width/2 - 250, game.height - 100, 'steelBone', 1, this.walls);
    addWall(game.width/2 - 125, game.height - 100, 'steelBone', 1, this.walls);
    addWall(game.width/2, game.height - 100, 'steelBone', 1, this.walls);
    addWall(game.width/2 + 125, game.height - 100, 'steelBone', 1, this.walls);
    addWall(game.width/2 + 250, game.height - 100, 'steelBone', 1, this.walls);
    setWallsPhysics(this.walls);

    game.physics.ninja.enableAABB([this.walls, this.player]);
  },
  update: function() {
    game.physics.ninja.collide(this.player, this.walls);
    moveByWASD(this.player, this.keys);
  }
}