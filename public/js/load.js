var loadState = {
  preload: function() {
    game.stage.backgroundColor = '#000000';

    var loadingLabel = game.add.text(game.width/2, game.height*0.4, 'loading...', {font:'30px Arial', fill:'#ffffff'});
    loadingLabel.anchor.setTo(0.5, 0.5);
    var progressBar = game.add.sprite(game.width/2, game.height*0.6, 'progressBar');
    progressBar.anchor.setTo(0.5, 0.5);
    game.load.setPreloadSprite(progressBar);

    console.log("loading assets...");
    // Walls
    game.load.image('steelBlock', "assets/images/wallblocks/steelBlock.png");
    game.load.image('steelBone', "assets/images/wallblocks/steelBone.png");
    // TileSet
    game.load.image('tileset', 'assets/images/tilesets/tileset.png');
    // TileMap
    game.load.tilemap('tilemap02', 'assets/mapJSONs/map2.json', null, Phaser.Tilemap.TILED_JSON);
    // GameObjects    
    game.load.image('coin', 'assets/images/gameObjects/coin.png');
    game.load.image('mine', 'assets/images/gameObjects/mine.png');
    game.load.image('cube', 'assets/images/gameObjects/bouncingCube.png');
    // Player
    game.load.spritesheet('playerIdle', "assets/images/player/spritesheets/IdleSheet.png", 24, 60, 5);
    game.load.spritesheet('playerWalk', "assets/images/player/spritesheets/WalkSheet.png", 26, 60, 2);
    game.load.spritesheet('playerRun', "assets/images/player/spritesheets/RunSheet.png", 32, 60, 3);
    game.load.spritesheet('playerFJump', "assets/images/player/spritesheets/FJumpSheet.png", 36, 60, 6);
    game.load.spritesheet('playerVJump', "assets/images/player/spritesheets/VJumpSheet.png", 36, 60, 6);
    game.load.spritesheet('playerWJump', "assets/images/player/spritesheets/WJumpSheet.png", 36, 60, 6);
    game.load.spritesheet('playerPickup', "assets/images/player/spritesheets/PickupSheet.png", 32, 60, 5);
    game.load.spritesheet('playerSheet', "assets/images/player/spritesheets/playerFull.png", 22, 30, 33);
  },
  create: function() {
    game.state.start('menu');
  }
}