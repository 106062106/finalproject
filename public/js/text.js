function setInputTexts() {
  var inputTexts = game.add.group();

  inputTexts.inputEnableChildren = true;
  inputTexts.onChildInputOver.add(hoverOver, this);
  inputTexts.onChildInputOut.add(hoverOut, this);

  return inputTexts;
}

function setText(posX, posY, text, font, group) {
  var item = game.add.text(posX, posY, text, font);

  item.anchor.setTo(0.5, 0.5);
  if (group != null) group.add(item);

  return item;
}

function hoverOver(item) {
  item.fill = '#ff0000';
}

function hoverOut(item) {
  item.fill = '#ffffff';
}