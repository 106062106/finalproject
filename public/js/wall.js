function initWalls() {
  var walls = game.add.group();

  walls.physicsBodyType = Phaser.Physics.NINJA;
  walls.enableBody = true;

  return walls;
}

function addWall(posX, posY, sprite, ratio, group) {
  var wall = game.add.sprite(posX, posY, sprite);
  
  wall.anchor.setTo(0.5, 0.5);
  wall.scale.setTo(ratio, ratio);
  if (group != null) group.add(wall);

  return wall;
}

function setWallsPhysics(group) {
  group.setAll('body.immovable', true);
  group.setAll('body.gravityScale', 0);
}
