// SETUP FUNCTIONS //
function setupPlayer(posX, posY, sprite) {
  var player = game.add.sprite(posX, posY, sprite);
  player.anchor.setTo(0.5, 0.5);

  setupAnimFull(player);

  game.physics.ninja.enableAABB(player);
  player.body.gravityScale = 0.7;
  player.body.bounce = 0.1;

  return player;
}

function setupAnimFull(player) {
  var anim = player.animations;
  anim.add('idle', [0, 1, 2, 1, 0, 3, 4, 3], 4, true);
  anim.add('walk', [6, 7], 4, true);
  anim.add('run', [8, 9], 8, true);
  anim.add('FJump', [10, 11, 12, 13, 14, 15], 3, false);
  anim.add('VJump', [16, 17, 18, 19, 20, 21], 3, false);
  anim.add('WJump', [22, 23, 24, 25, 26, 27], 3, false);
  anim.add('fall', [14], 1, false);
  anim.add('slide', [22], 1, false);
  anim.add('pickUp', [28, 29, 30, 31, 32, 31, 30, 29, 28], 3, false);
}

function setupAnimSep(player) {
  player.animations.add('idle', [0, 1, 2, 1, 0, 3, 4, 3], 4, true);
  player.animations.play('idle');
}

function setupWASD() {
  var keys = {};

  keys.W = game.input.keyboard.addKey(Phaser.Keyboard.W);
  keys.A = game.input.keyboard.addKey(Phaser.Keyboard.A);
  keys.S = game.input.keyboard.addKey(Phaser.Keyboard.S);
  keys.D = game.input.keyboard.addKey(Phaser.Keyboard.D);

  return keys;
}

// UPDATE FUNCTIONS //
function moveByWASD(player, keys, tiles) {
  var up = keys.W.isDown;
  var left = keys.A.isDown;
  var right = keys.D.isDown;
  var body = player.body;

  slopeJumpCheck(player, tiles);
  playerSpdCtrl(up, left, right, player);
  playerAnim_Full(up, left, right, player);

  body.moveUp(body.velocity.y*4);
  body.moveRight(body.velocity.x/6);
}

function moveByCursors(player, cursors, tiles) {
  var up = cursors.up.isDown;
  var left = cursors.left.isDown;
  var right = cursors.right.isDown;
  var body = player.body;

  slopeJumpCheck(player, tiles);
  playerSpdCtrl(up, left, right, player);
  playerAnim_Full(up, left, right, player);

  body.moveUp(body.velocity.y*4);
  body.moveRight(body.velocity.x/6);
}

// MOVEMENT FUNCTIONS //
function slopeJumpCheck(player, tiles) {
  player.slopeJump = false;
  if (tiles != null) {
    for (var i = 0; i < tiles.length; i++) {
      if (player.body.aabb.collideAABBVsTile(tiles[i].tile) != 0) {
        switch (tiles[i].tile.id) {
          case 2:
          case 3:
            player.slopeJump = true;
            break;
        } 
      }
    }
  }
}

function playerSpdCtrl(up, left, right, player) {
  var Vy = player.body.velocity.y;
  var Vx = player.body.velocity.x;
  var touch = player.body.touching;
  var dirX = right - left;

  if (up) {
    if (player.slopeJump || touch.down || touch.left && left || touch.right && right) Vy = 15;
    else if (Vy > 0) Vy--;
  } else {
    Vy = 0;
  }
  switch(dirX) {
    case -1:
      if (Vx > 0) Vx = 0;
      else if (Vx > -60) Vx--;
      else Vx = -60;
      break;
    case 0:
      if (Vx < 0) Vx++;
      if (Vx > 0) Vx--;
      break;
    case 1:
      if (Vx < 0) Vx = 0;
      else if (Vx < 60) Vx++;
      else Vx = 60;
      break;
  }
  player.body.velocity.x = Vx;
  player.body.velocity.y = Vy;
}

// ANIMATION FUNCTIONS //
function playerAnim_Full(up, left, right, player) {
  var touch = player.body.touching;
  var anim = player.animations;
  var Vx = player.body.velocity.x;
  var Vy = player.body.velocity.y;

  if (up) {
    if (left && touch.left || right && touch.right) {
      if (left) Vx = 200;
      if (right) Vx = -200;
      anim.play('WJump');
    }
    else if (touch.down) {
      if (Vx != 0) anim.play('FJump');
      else anim.play('VJump');
    }
  } else {
    if (touch.down) {
      if (Vx == 0) anim.play('idle');
      else if (Vx > 0) {
        if (player.scale.x < 0) player.scale.x *= -1;
        if (Vx > 30) anim.play('run');
        else anim.play('walk');
      }
      else if (Vx < 0) {
        if (player.scale.x > 0) player.scale.x *= -1;
        if (Vx < -30) anim.play('run');
        else anim.play('walk');
      }
    } else {
      if (Vx > 0 && player.scale.x < 0) player.scale.x *= -1;
      if (Vx < 0 && player.scale.x > 0) player.scale.x *= -1;
      if (anim.currentAnim.isFinished) anim.play('fall');
    }
    if (touch.left && left || touch.right && right) {
      anim.play('slide');
      if (Vy < 1 && !touch.up) Vy = 1;
    }
  }
  player.body.velocity.x = Vx;
  player.body.velocity.y = Vy;
}

function playerAnim_Sep(up, left, right, player) {
  var touch = player.body.touching;
  var anim = player.animations;
  var Vx = player.body.velocity.x;
  var Vy = player.body.velocity.y;

  if (up) {
    if (left && touch.left || right && touch.right) {
      if (left) Vx = 200;
      if (right) Vx = -200;
      player.loadTexture('playerWJump');
      anim.add('WJump', [0, 1, 2, 3, 4, 5], 6, false);
      anim.play('WJump');
    }
    else if (touch.down) {
      if (Vx != 0) {
        player.loadTexture('playerFJump');
        anim.add('FJump', [0, 1, 2, 3, 4, 5], 3, false);
        anim.play('FJump');
      } else {
        player.loadTexture('playerVJump');
        anim.add('VJump', [0, 1, 2, 3, 4, 5], 3, false);
        anim.play('VJump');
      }
    }
  } else {
    if (touch.down) {
      if (Vx == 0) {
        if (anim.currentAnim.name != 'idle') {
          player.loadTexture('playerIdle');
          anim.add('idle', [0, 1, 2, 1, 0, 3, 4, 3], 4, true);
          anim.play('idle');
        }
      }
      else if (Vx > 0) {
        if (player.scale.x < 0) player.scale.x *= -1;
        if (Vx > 30) {
          if (anim.currentAnim.name != 'run') {
            player.loadTexture('playerRun');
            anim.add('run', [1, 2], 8, true);
            anim.play('run');
          }
        } else {
          if (anim.currentAnim.name != 'walk') {
            player.loadTexture('playerWalk');
            anim.add('walk', [0, 1], 4, true);
            anim.play('walk');
          }
        }
      }
      else if (Vx < 0) {
        if (player.scale.x > 0) player.scale.x *= -1;
        if (Vx < -30) {
          if (anim.currentAnim.name != 'run') {
            player.loadTexture('playerRun');
            anim.add('run', [1, 2], 8, true);
            anim.play('run');
          }
        } else {
          if (anim.currentAnim.name != 'walk') {
            player.loadTexture('playerWalk');
            anim.add('walk', [0, 1], 4, true);
            anim.play('walk');
          }
        }
      }
    } else {
      if (Vx > 0 && player.scale.x < 0) player.scale.x *= -1;
      if (Vx < 0 && player.scale.x > 0) player.scale.x *= -1;
    }
    if (touch.left && left || touch.right && right) {
      if (Vy < 1 && !touch.up) Vy = 1;
    }
  }
  player.body.velocity.x = Vx;
  player.body.velocity.y = Vy;
}