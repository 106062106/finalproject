var bootState = {
  preload: function() {
    game.stage.backgroundColor = '#000000';
    game.load.image('progressBar', "assets/images/system/progressBar.png");

    console.log('boot system...');
    game.physics.startSystem(Phaser.Physics.NINJA);
    game.renderer.renderSession.roundPixels = true;
  },
  create: function() {
    game.state.start('load');
  }
}