var map02 = { 
  create: function(){
    this.game.stage.backgroundColor = "#ffffff";
 
    this.map = game.add.tilemap('tilemap02');
    this.map.addTilesetImage('tileset');
    this.Layer1 = this.map.createLayer('Layer1');
    this.Layer1.resizeWorld();
 
    this.slopeMap = {'1': 1, '2': 2, '3': 5, '4': 4, '5': 3, '6': 0, '7': 0, '8': 0};
    this.tiles = game.physics.ninja.convertTilemap(this.map, this.Layer1, this.slopeMap);

    this.player = setupPlayer(150, 540, 'playerSheet');
 
    cursors = game.input.keyboard.createCursorKeys();

    this.coins = game.add.group();
    this.coins.physicsBodyType = Phaser.Physics.NINJA;
    this.coins.enableBody = true;
    this.map.createFromObjects('Object Layer 1', 1, 'coin', 0, true, false, this.coins);
    this.coins.setAll('body.gravityScale', 0);
    this.coins.setAll('body.immovable', true);

    this.mines = game.add.group();
    this.mines.physicsBodyType = Phaser.Physics.NINJA;
    this.mines.enableBody = true;
    this.map.createFromObjects('Object Layer 2', 7, 'mine', 0, true, false, this.mines);
    this.mines.setAll('body.gravityScale', 0);
    this.mines.setAll('body.immovable', true);

    /*this.enemies = game.add.group();
    this.enemies.physicsBodyType = Phaser.Physics.NINJA;
    this.enemies.enableBody = true;
    this.enemies.createMultiple(20,'enemies');
    this.enemies.setAll('body.gravityScale', 0);
    this.DrawMap2enemies();
    this.enemies.forEachAlive(function(child){
      child.positionx = child.x;
      child.positiony = child.y;
    });*/

    this.cubes = game.add.group();
    this.cubes.physicsBodyType = Phaser.Physics.NINJA;
    this.cubes.enableBody = true;
    this.map.createFromObjects('Object Layer 3', 6, 'cube', 0, true, false, this.cubes);
    this.cubes.setAll('body.gravityScale', 0);
    this.cubes.setAll('body.bounce', 0);
    this.cubePos = [{x: 640, y: 240}, {x:740, y: 350}, {x: 740, y: 440}, {x: 670, y: 520}, 
                    {x: 510, y: 580}, {x: 410, y: 490}, {x: 440, y: 390}, {x: 520, y: 290}];
    this.cubeAttached = [0, 0, 0, 0, 0, 0, 0, 0];
    this.vYstore = [0, 0, 0, 0, 0, 0, 0, 0];
    this.vXstore = [0, 0, 0, 0, 0, 0, 0, 0];
  },
 
  update: function(){
    for (var i = 0; i < this.tiles.length; i++) {
      this.player.body.aabb.collideAABBVsTile(this.tiles[i].tile);
    }
    
    this.takeCoin();

    this.takeMine();

    this.touchCube();

    //this.Patrol();

    moveByCursors(this.player, cursors, this.tiles);
  },

  Patrol: function(){
    this.enemies.forEach(function(child){
      if(child.TWO){
        //child move between position and patrol
        if(child.positiony==child.patroly){
          if(child.x==child.positionx){
            child.state = 1;
          }
          else if(child.state==1&&((child.positionx<child.patrolx&&child.x>child.patrolx)||(child.positionx>child.patrolx&&child.x<child.patrolx))){
            child.state = 2;
          }
          else if(child.x==child.patrolx){
            child.state = 3;
          }
          else if(child.state==3&&((child.positionx<child.patrolx&&child.x<child.positionx)||(child.positionx>child.patrolx&&child.x>child.positionx))){
            child.state = 4;
          }
        
          if(child.state==1){
            if(child.patrolx>child.positionx){
              child.body.moveRight(400);
            }
            else if(child.patrolx<child.positionx){
              child.body.moveLeft(400);
            }
          }
          else if(child.state==2){
              child.reset(child.patrolx,child.patroly);
          }
          else if(child.state==3){
            if(child.patrolx>child.positionx){
              child.body.moveLeft(400);
            }
            else if(child.patrolx<child.positionx){
              child.body.moveLeft(400);
            }
          }
          else if(child.state==4){
            child.reset(child.positionx,child.positiony);
          }
        }
        if(child.positionx==child.patrolx){
          if(child.y==child.positiony){
            child.state = 1;
          }
          else if(child.state==1&&((child.positiony<child.patroly&&child.y>child.patroly)||(child.positiony>child.patroly&&child.y<child.patroly))){
            child.state = 2;
          }
          else if(child.y==child.patroly){
            child.state = 3;
          }
          else if(child.state==3&&((child.positiony<child.patroly&&child.y<child.positiony)||(child.positiony>child.patroly&&child.y>child.positiony))){
            child.state = 4;
          }
        
          if(child.state==1){
            if(child.patroly>child.positiony){
              child.body.moveDown(400);
            }
            else if(child.patroly<child.positiony){
              child.body.moveUp(400);
            }
          }
          else if(child.state==2){
              child.reset(child.patrolx,child.patroly);
          }
          else if(child.state==3){
            if(child.patroly>child.positiony){
              child.body.moveUp(400);
            }
            else if(child.patroly<child.positiony){
              child.body.moveDown(400);
            }
          }
          else if(child.state==4){
            child.reset(child.positionx,child.positiony);
          }
        }
          // console.log(child+' inpatrol between'+'{'+child.x+','+child.y+'} and {'+child.patrolx+','+child.patroly+'}');
      }
      else{
       // move between four points clockwise
        if(child.x==child.positionx&&child.y==child.positiony){
          child.state = 2;
        }
        else if(child.state==2&&((child.patrolx>child.positionx&&child.x>child.patrolx)||(child.patroly>child.positiony&&child.y>child.patroly)||(child.patrolx<child.positionx&&child.x<child.patrolx)||(child.patroly<child.positiony&&child.y<child.patroly))){
          child.state = 3;
        }
        else if(child.x==child.patrolx&&child.y==child.patroly){
          child.state = 4;
        }
        else if(child.state==4&&((child.patrolx1>child.patrolx&&child.x>child.patrolx1)||(child.patroly1>child.patroly&&child.y>child.patroly1)||(child.patrolx1<child.patrolx&&child.x<child.patrolx1)||(child.patroly1<child.patroly&&child.y<child.patroly1))){
          child.state = 5;
        }
        else if(child.x==child.patrolx1&&child.y==child.patroly1){
          child.state = 6;
        }
        else if(child.state==6&&((child.patrolx2>child.patrolx1&&child.x>child.patrolx2)||(child.patroly2>child.patroly1&&child.y>child.patroly2)||(child.patrolx2<child.patrolx1&&child.x<child.patrolx2)||(child.patroly2<child.patroly1&&child.y<child.patroly2))){
          child.state = 7;
        }
        else if(child.x==child.patrolx2&&child.y==child.patroly2){
          child.state = 8;
        }
        else if(child.state==8&&((child.positionx>child.patrolx2&&child.x>child.positionx)||(child.positiony>child.patroly2&&child.y>child.positiony)||(child.positionx<child.patrolx2&&child.x<child.positionx)||(child.positiony<child.patroly2&&child.y<child.patroly2))){
          child.state = 1;
        }


        if(child.state == 1){
          child.reset(child.positionx,child.positiony);
        }
        else if(child.state==2){
          if(child.positionx<child.patrolx&&child.positiony==child.patroly){
            child.body.moveRight(400);
          }
          else if(child.positionx>child.patrolx&&child.positiony==child.patroly){
            child.body.moveLeft(400);
          }
          else if(child.positionx==child.patrolx&&child.positiony<child.patroly){
            child.body.moveDown(400);
          }
          else if(child.positionx==child.patrolx&&child.positiony>child.patroly){
            child.body.moveUp(400);
          }
        }
        else if(child.state==3){
          child.reset(child.patrolx,child.patroly);
        }
        else if(child.state==4){
          if(child.patrolx<child.patrolx1&&child.patroly==child.patroly1){
            child.body.moveRight(400);
          }
          else if(child.patrolx>child.patrolx1&&child.patroly==child.patroly1){
            child.body.moveLeft(400);
          }
          else if(child.patrolx==child.patrolx1&&child.patroly<child.patroly1){
            child.body.moveDown(400);
          }
          else if(child.patrolx==child.patrolx1&&child.patroly>child.patroly1){
            child.body.moveUp(400);
          }
        }
        else if(child.state==5){
          child.reset(child.patrolx1,child.patroly1);
        }
        else if(child.state==6){
          if(child.patrolx1<child.patrolx2&&child.patroly1==child.patroly2){
            child.body.moveRight(400);
          }
          else if(child.patrolx1>child.patrolx2&&child.patroly1==child.patroly2){
            child.body.moveLeft(400);
          }
          else if(child.patrolx1==child.patrolx2&&child.patroly1<child.patroly1){
            child.body.moveDown(400);
          }
          else if(child.patrolx1==child.patrolx2&&child.patroly1>child.patroly2){
            child.body.moveUp(400);
          }
        }
        else if(child.state==7){
          child.reset(child.patrolx2,child.patroly2);
        }
        else if(child.state==8){
          if(child.patrolx2<child.positionx&&child.patroly2==child.positiony){
            child.body.moveRight(400);
          }
          else if(child.patrolx2>child.positionx&&child.patroly2==child.positiony){
            child.body.moveLeft(400);
          }
          else if(child.patrolx2==child.positionx&&child.patroly2<child.positiony){
            child.body.moveDown(400);
          }
          else if(child.patrolx2==child.positionx&&child.patroly2>child.positiony){
            child.body.moveUp(400);
          }
        }
      }
    });
  },

  DrawMap2enemies: function(){
    var enemy1 = this.enemies.getFirstDead();
    if (!enemy1) { return;}
    enemy1.reset(30,270);
    enemy1.patrolx = 110;
    enemy1.patroly = 270;
    enemy1.TWO = true;
    
    var enemy2 = this.enemies.getFirstDead();
    if (!enemy2) { return;}
    enemy2.reset(80,340);
    enemy2.patrolx = 80;
    enemy2.patroly = 430;
    enemy2.TWO = true;
          
    var enemy3 = this.enemies.getFirstDead();
    if (!enemy3) { return;}
    enemy3.reset(110,490);
    enemy3.patrolx = 110;
    enemy3.patroly = 630;
    enemy3.TWO = true;
    
    var enemy4 = this.enemies.getFirstDead();
    if (!enemy4) { return;}
    enemy4.reset(170,250);
    enemy4.patrolx = 240;
    enemy4.patroly = 250;
    enemy4.patrolx1 = 240;
    enemy4.patroly1 = 330;
    enemy4.patrolx2 = 170;
    enemy4.patroly2 = 330;
    enemy4.TWO = false;
    
    var enemy5 = this.enemies.getFirstDead();
    if (!enemy5) { return;}
    enemy5.reset(170,410);
    enemy5.patrolx = 240;
    enemy5.patroly = 410;
    enemy5.patrolx1 = 240;
    enemy5.patroly1 = 490;
    enemy5.patrolx2 = 170;
    enemy5.patroly2 = 490;
    enemy5.TWO = false;
    
    var enemy6 = this.enemies.getFirstDead();
    if (!enemy6) { return;}
    enemy6.reset(170,570);
    enemy6.patrolx = 170;
    enemy6.patroly = 660;
    enemy6.patrolx1 = 240;
    enemy6.patroly1 = 660;
    enemy6.patrolx2 = 240;
    enemy6.patroly2 = 570;
    enemy6.TWO = false;
    
    var enemy7 = this.enemies.getFirstDead();
    if (!enemy7) { return;}
    enemy7.reset(300,250);
    enemy7.patrolx = 300;
    enemy7.patroly = 330;
    enemy7.patrolx1 = 360;
    enemy7.patroly1 = 330;
    enemy7.patrolx2 = 360;
    enemy7.patroly2 = 250;
    enemy7.TWO = false;
    
    var enemy8 = this.enemies.getFirstDead();
    if (!enemy8) { return;}
    enemy8.reset(360,490);
    enemy8.patrolx = 300;
    enemy8.patroly = 490;
    enemy8.patrolx1 = 300;
    enemy8.patroly1 = 410;
    enemy8.patrolx2 = 360;
    enemy8.patroly2 = 410;
    enemy8.TWO = false;

    
    var enemy9 = this.enemies.getFirstDead();
    if (!enemy9) { return;}
    enemy9.reset(300,660);
    enemy9.patrolx = 360;
    enemy9.patroly = 660;
    enemy9.patrolx1 = 360;
    enemy9.patroly1 = 570;
    enemy9.patrolx2 = 300;
    enemy9.patroly2 = 570;
    enemy9.TWO = false;
    
    var enemy10 = this.enemies.getFirstDead();
    if (!enemy10) { return;}
    enemy10.reset(340,710);
    enemy10.patrolx = 470;
    enemy10.patroly = 710;
    enemy10.TWO = true;
    
    var enemy11 = this.enemies.getFirstDead();
    if (!enemy11) { return;}
    enemy11.reset(420,280);
    enemy11.patrolx = 420;
    enemy11.patroly = 430;
    enemy11.TWO = true;
    
    var enemy12 = this.enemies.getFirstDead();
    if (!enemy12) { return;}
    enemy12.reset(490,140);
    enemy12.patrolx = 490;
    enemy12.patroly = 630;
    enemy12.TWO = true;
    
    var enemy13 = this.enemies.getFirstDead();
    if (!enemy13) { return;}
    enemy13.reset(570,210);
    enemy13.patrolx = 570;
    enemy13.patroly = 650;
    enemy13.TWO = true;
    
    var enemy14 = this.enemies.getFirstDead();
    if (!enemy14) { return;}
    enemy14.reset(640,650);
    enemy14.patrolx = 640;
    enemy14.patroly = 210;
    enemy14.TWO = true;
    
    var enemy15 = this.enemies.getFirstDead();
    if (!enemy15) { return;}
    enemy15.reset(710,330);
    enemy15.patrolx = 710;
    enemy15.patroly = 660;
    enemy15.TWO = true;
    
    var enemy16 = this.enemies.getFirstDead();
    if (!enemy16) { return;}
    enemy16.reset(840,280);
    enemy16.patrolx = 910;
    enemy16.patroly = 280;
    enemy16.patrolx1 = 910;
    enemy16.patroly1 = 360;
    enemy16.patrolx2 = 840;
    enemy16.patroly2 = 360;
    enemy16.TWO = false;
    
    var enemy17 = this.enemies.getFirstDead();
    if (!enemy17) { return;}
    enemy17.reset(910,450);
    enemy17.patrolx = 910;
    enemy17.patroly = 610;
    enemy17.TWO = true;
  },

  takeCoin: function(){
    var playerX = this.player.x;
    var playerY = this.player.y;
    //game.physics.ninja.overlap(this.player, this.coins, this.takeCoin, null, this);
    this.coins.forEach(function(child){
      if(Math.abs(playerX - child.x) <= 21 && Math.abs(playerY - child.y) <= 21){
        child.kill();
      }
    });
  },

  takeMine: function(){
    var playerX = this.player.x;
    var playerY = this.player.y;
    var player = this.player;
    //game.physics.ninja.overlap(this.player, this.coins, this.takeCoin, null, this);
    this.mines.forEach(function(child){
      if(Math.abs(playerX - child.x) <= 31 && Math.abs(playerY - child.y) <= 31){
        player.kill();
        game.state.start('map02');
      }
    });
  },

  touchCube: function(){
    var playerX = this.player.body.x;
    var playerY = this.player.body.y;
    var player = this.player;
    var cubes = this.cubes;
    var max = 16;
    var cubeAttached = this.cubeAttached;
    var vYstore = this.vYstore;
    var vXstore = this.vXstore;
    var pos = this.cubePos;
    //game.physics.ninja.overlap(this.player, this.coins, this.takeCoin, null, this);
    this.cubes.forEach(function(child){
      var vY = vYstore[cubes.getIndex(child)];
      var vX = vXstore[cubes.getIndex(child)];

      if(Math.abs(playerX - child.body.x) <= 45 && Math.abs(playerY - child.body.y) <= 45){
        if(cubeAttached[cubes.getIndex(child)] == 0){
          //child.body.gravityScale = 0.3;

          if(playerY < child.body.y-15 && playerX > child.body.x-15 && playerX < child.body.x+15){
              vY = max;
              child.body.velocity.y = max;
          }
          else if(playerY > child.body.y+15 && playerX > child.body.x-15 && playerX < child.body.x+15){
              vY = -max;
              child.body.velocity.y = -max;
          }
          else if(playerX < child.body.x-15 && playerY > child.body.y-15 && playerY < child.body.y+15){
              vX = max;
              child.body.velocity.x = max;
          }
          else if(playerX > child.body.x+15 && playerY > child.body.y-15 && playerY < child.body.y+15){
              vX = -max;
              child.body.velocity.x = -max;
          }
        }
        cubeAttached[cubes.getIndex(child)] = 1;
      }
      else{
        cubeAttached[cubes.getIndex(child)] = 0;
      }
      child.body.moveDown(vY*4);

      if(vY > 0){
        vY--;
      }
      else if(vY < 0){
        vY++;
      }

      if(Math.abs(vY) < 1){
        vY = -child.body.velocity.y;
        if(Math.abs(child.body.velocity.y) <= 1){
          child.body.velocity.y = 0;
          vY = 0;
        }
        else{
          child.body.velocity.y *= -0.9;
          //vY = child.body.velocity.y;
        }
      }

      vYstore[cubes.getIndex(child)] = vY;
    });
  }
}