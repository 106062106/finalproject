var menuState = {
  preload: function() {
    game.stage.backgroundColor = '#000000';
    this.inputTexts = setInputTexts();
  },
  create: function() {
    var cntX = game.width/2;
    var cntY = game.height/2;
    var font = {font:'bold 25px Arial', fill:'#ffffff'};

    var startText = setText(cntX, cntY - 50, 'start', font, this.inputTexts);
    var contText = setText(cntX, cntY, 'continue', font, this.inputTexts);
    var setsText = setText(cntX, cntY + 50, 'settings', font, this.inputTexts);
    var helpText = setText(cntX, cntY + 100, 'tutorial', font, this.inputTexts);

    startText.events.onInputDown.add(this.start, this);
    contText.events.onInputDown.add(this.continue, this);
    setsText.events.onInputDown.add(this.setting, this);
    helpText.events.onInputDown.add(this.help, this);
  },
  start: function() {
    console.log("start game...");
    game.state.start('map02');
  },
  continue: function() {
    console.log("continued...");
  },
  setting: function() {
    console.log("To setting...");
  },
  help: function() {
    console.log("To tutorial...");
    game.state.start('tutorial');
  }
}