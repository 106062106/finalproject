# FinalProject
I. Completed Tasks
  A. Player Spritesheets:
    1. Idle Animation
      a. Idle
    2. Move Animation
      a. Walk
      b. Run
      c. Crowl
    3. Jump Animation
      a. Vertical
      b. Forward
      c. Wall
    4. Interactive Animation
      a. Pickup
    5. Offensive Animation
      a. Slash - melee
      b. Throw - ranged
  B. Player Sprite Parts
    1. Offensive Pose
      a. CouchFire
      b. StandFire
    2. Free Arm
      a. handgun
      b. longgun

II. Queued Tasks
  A. States
    1. Boot
    2. Load
    3. Menu
  B. Global Funcitons
    1. Pause
    2. Mute
  C. Player Spritesheet
